const through = require("through2")
const Path = require("path")

const manifest = require("./../manifest")
const hash = require("./../hash")

module.exports = ({ dirname, entry }) =>
  through.obj(async function(file, enc, callback) {
    const fileHash = await hash(entry)
    await manifest(dirname, "css", `bundle.${fileHash}.css`)

    const base = Path.dirname(entry)
    const extname = Path.extname(entry)
    file.path = `${base}/bundle.${fileHash}${extname}`
    this.push(file)
    callback()
  })

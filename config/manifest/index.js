const promisify = require("promisify-node")
const Path = require("path")
const Fs = promisify("fs")
const Fse = require("fs-extra")

module.exports = async (dirname, key, value) => {
  const path = Path.join(dirname, "dist")
  const file = `${path}/MANIFEST.json`
  let json = null

  // ensure path exists and create if doesn't
  // prevents Fs.access from throwing
  await Fse.ensureDir(path).catch(err => console.log(err))

  // chack if file exists
  // create empty one if not
  await Fs.access(file, Fs.constants.F_OK).catch(async () => {
    await Fs.writeFile(
      file,
      JSON.stringify({ js: "bundle.js", css: "bundle.css" })
    ).catch(err => console.log(err))
  })

  // read the json from the file
  await Fse.readJson(file)
    .then(data => (json = data))
    .catch(err => console.log(err))

  // write updated json to file
  json[key] = value
  Fse.outputJson(file, json).catch(err => console.log(err))
}
